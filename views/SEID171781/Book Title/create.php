<?php
if (!session_start()) session_start();
require_once ("../../../vendor/autoload.php");

use App\Message\Message;

//$msg = Message::message();

//echo "<div><div align='center' class='alert-info' id='message'> $msg </div></div>"

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title Entry Information</title>
    <link rel="stylesheet" href="../../../resources/bootstrap/css/formstyle.css">
    <link rel="stylesheet" href="../../../resources/bootstrap/css/bootstrap-theme.css">
    <script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resources/bootstrap/js/jquery.js"></script>

    <style>
        body{
            background-color: #5bc0de;
        }

    </style>

</head>
<body>
        <div class="container">
            <h1 style="background-color: #3e8f3e" align="center">Book Title Entry Information</h1>
            <form action="store.php" method="post">
                <strong>Book Name:</strong>
                  <input type="text" name="bookTitle">
                <strong>Author Name:</strong>
                <input type="text" name="authorName">
                <input type="submit" value="Submit">
            </form>

        </div>
</body>
</html>
